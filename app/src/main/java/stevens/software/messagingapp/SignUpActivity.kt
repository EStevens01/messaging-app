package stevens.software.messagingapp

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.UserProfileChangeRequest
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_signup.*


class SignUpActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)
        auth = FirebaseAuth.getInstance()

        val database = FirebaseDatabase.getInstance()
        val databaseReference = database.reference

        signUpButton.setOnClickListener{
            val email = emailField.text.toString()
            val password = passwordField.text.toString()
            val fullName = nameField.text.toString()

            auth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(this) {task ->
                if(task.isSuccessful){
                    val user = auth.currentUser
                    val newUser = User(fullName)
                    databaseReference.child("users").child(user!!.uid).setValue(newUser)

                    updateUserDisplayName()
                }else{
                     Toast.makeText(baseContext, "Authentication failed.", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun updateUserDisplayName(){
        val user = auth.currentUser
        val fullName = nameField.text.toString()

        val profileName = UserProfileChangeRequest.Builder().setDisplayName(fullName).build()

        user?.updateProfile(profileName)
            ?.addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    startMessagesActivity()
                }
            }
    }

    private fun startMessagesActivity(){
        val activityIntent = Intent(this, MessagesActivity::class.java)
        startActivity(activityIntent)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
}
