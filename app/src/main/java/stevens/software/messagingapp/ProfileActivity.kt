package stevens.software.messagingapp

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import kotlinx.android.synthetic.main.activity_profile.*

class ProfileActivity : AppCompatActivity(){

    private lateinit var auth: FirebaseAuth
    private var currentUser: FirebaseUser? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        auth = FirebaseAuth.getInstance()
        currentUser = auth.currentUser

        userName.text = currentUser?.displayName
        usersEmail.text = currentUser?.email


        suggestedFriends.setOnClickListener{
            val activityIntent = Intent(this, SuggestedFriendsActivity::class.java)
            startActivity(activityIntent)
        }

        friendRequests.setOnClickListener{
            val activityIntent = Intent(this, FriendRequestsActivity::class.java)
            startActivity(activityIntent)
        }

        friends.setOnClickListener{
            val activityIntent = Intent(this, FriendsListActivity::class.java)
            startActivity(activityIntent)
        }


    }



}