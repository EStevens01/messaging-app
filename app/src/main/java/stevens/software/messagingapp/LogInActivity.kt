package stevens.software.messagingapp

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_login.*

class LogInActivity : AppCompatActivity(){

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        auth = FirebaseAuth.getInstance()

        signUpLink.setOnClickListener{
            val activityIntent = Intent(this, SignUpActivity::class.java)
            startActivity(activityIntent)
        }

        loginButton.setOnClickListener{
            val email = loginEmailField.text.toString()
            val password = loginPasswordField.text.toString()

            auth.signInWithEmailAndPassword(email, password).addOnCompleteListener{task ->
                if(task.isSuccessful){
                    val activityIntent = Intent(this, ProfileActivity::class.java)
                    startActivity(activityIntent)
                }else{
                    Toast.makeText(baseContext, "Authentication failed.", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }
}

