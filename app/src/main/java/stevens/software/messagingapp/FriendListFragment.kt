package stevens.software.messagingapp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*

class FriendListFragment : Fragment() {
    private lateinit var auth: FirebaseAuth
    private lateinit var database: FirebaseDatabase
    private lateinit var friendRequestDatabase: DatabaseReference
    private lateinit var friendshipsDatabase: DatabaseReference
    private val friendRequestActivityClassName = "FriendRequestsActivity"
    private val friendsListActivityClassName = "FriendsListActivity"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.friend_list_fragment, container, false)
        val activity = activity?.javaClass?.simpleName

        database = FirebaseDatabase.getInstance()
        auth = FirebaseAuth.getInstance()
        friendRequestDatabase = FirebaseDatabase.getInstance().reference.child("friend_requests")
        friendshipsDatabase = FirebaseDatabase.getInstance().reference.child("friendships")

        val friendsList : RecyclerView = view.findViewById(R.id.friendListFragment)
        friendsList.layoutManager = LinearLayoutManager(context)


            class FriendViewHolder(friendItemView: View) : RecyclerView.ViewHolder(friendItemView) {
                var userName: TextView = friendItemView.findViewById(R.id.userName)
                var addFriendButton: ImageButton = friendItemView.findViewById(R.id.addFriendButton)
                var acceptRequestButton : Button = friendItemView.findViewById(R.id.acceptRequestButton)
           }

        val query = when{
            activity.equals(friendRequestActivityClassName) -> databaseRequestsReceivedQuery(auth.currentUser?.uid!!)
            activity.equals(friendsListActivityClassName) -> databaseFriendshipsQuery(auth.currentUser?.uid!!)
            else -> {
                databaseSuggestedUsersQuery()
            }
        }

        val options: FirebaseRecyclerOptions<User> = FirebaseRecyclerOptions.Builder<User>()
            .setLifecycleOwner(this)
            .setQuery(query, User::class.java)
            .build()

        val firebaseRecyclerAdapter = object : FirebaseRecyclerAdapter<User, FriendViewHolder>(options){
            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FriendViewHolder {
                val friendItemView: View = LayoutInflater.from(parent.context).inflate(R.layout.friend_list_item, parent, false)
                return FriendViewHolder(friendItemView)
            }

            override fun onBindViewHolder(holder: FriendViewHolder, position: Int, model: User) {
                val userId = getRef(position).key
                val currentUser = auth.currentUser?.uid
                holder.userName.text = model.name

                if(activity.equals(friendRequestActivityClassName) or activity.equals(friendsListActivityClassName)){
                    holder.addFriendButton.isVisible = false
                    holder.acceptRequestButton.isVisible = true

                    if(activity.equals(friendsListActivityClassName)){
                        holder.acceptRequestButton.isVisible = false
                    }
                    val friendRequestQuery = FirebaseDatabase.getInstance().reference.child("users").child(userId!!)
                    friendRequestQuery.addValueEventListener(object : ValueEventListener {
                        override fun onCancelled(p0: DatabaseError) {
                        }

                        override fun onDataChange(dataSnapshot: DataSnapshot) {
                            for (data in dataSnapshot.children) {
                                holder.userName.text = data.value as CharSequence?
                            }
                        }

                    })
                }

                holder.addFriendButton.setOnClickListener{
                    friendRequestDatabase.child(currentUser!!).child(userId!!).child("request_type").setValue("sent").addOnCompleteListener { task ->
                        if(task.isSuccessful){
                            friendRequestDatabase.child(userId).child(currentUser).child("request_type").setValue("received").addOnSuccessListener {
                               Toast.makeText(context, "friend request sent", Toast.LENGTH_SHORT).show()
                                holder.addFriendButton.setImageResource(R.drawable.ic_close_black_24dp)
                            }
                        }else{
                            Toast.makeText(context, "friend request failed to send", Toast.LENGTH_SHORT).show()
                        }
                    }
                }

                holder.acceptRequestButton.setOnClickListener{
                    friendshipsDatabase.child(currentUser!!).child(userId!!).child("plz").setValue(true).addOnCompleteListener { task ->
                        if (task.isSuccessful){
                            friendshipsDatabase.child(userId).child(currentUser).child("plz").setValue(true).addOnCompleteListener { n ->
                                if(n.isSuccessful){
                                    friendRequestDatabase.child(currentUser).child(userId).removeValue().addOnCompleteListener {
                                        friendsList.adapter?.notifyItemRemoved(position)
                                        friendsList.adapter?.notifyDataSetChanged()
                                    }
                                    friendRequestDatabase.child(userId).child(currentUser).removeValue().addOnCompleteListener {
                                        friendsList.adapter?.notifyItemRemoved(position)
                                        friendsList.adapter?.notifyDataSetChanged()
                                    }

                                }
                            }
                        }
                    }
                }

            }
        }
        friendsList.adapter = firebaseRecyclerAdapter
        return view
    }

    private fun databaseSuggestedUsersQuery() : Query{
        return FirebaseDatabase.getInstance()
            .reference
            .child("users")
            .orderByChild("name")
            .limitToLast(50)
    }

    private fun databaseRequestsReceivedQuery(currentUser : String) : Query{
        return FirebaseDatabase.getInstance()
            .reference
            .child("friend_requests")
            .child(currentUser)
            .orderByChild("request_type").equalTo("received")
            .limitToLast(50)
    }

    private fun databaseFriendshipsQuery(currentUser : String) : Query{
        return FirebaseDatabase.getInstance()
            .reference
            .child("friendships")
            .child(currentUser)
            .limitToLast(50)
    }
}