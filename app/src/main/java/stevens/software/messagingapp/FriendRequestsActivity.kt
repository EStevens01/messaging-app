package stevens.software.messagingapp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_friends_list.*


class FriendRequestsActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    lateinit var currentUser: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_friends_list)
        auth = FirebaseAuth.getInstance()
        currentUser = auth.currentUser!!.uid

        friendListTitle.text = "Friend Requests"
    }

}


//                    friendDatabase.child(currentUser).child(userId!!).setValue(true).addOnSuccessListener {
//                        friendDatabase.child(newUser!!).child(currentUser).setValue(true).addOnSuccessListener {
//                            Toast.makeText(this@FriendListActivity, "adddded", Toast.LENGTH_SHORT).show()
//                        }
//                    }


//                    friendRequestDatabase.child(currentUser!!).child(userId!!).child("request_type").setValue("sent").addOnCompleteListener { task ->
//                        if(task.isSuccessful){
//                            friendRequestDatabase.child(userId).child(currentUser).child("request_type").setValue("received").addOnSuccessListener {
//                                Toast.makeText(this@SuggestedFriendsActivity, "friend request sent", Toast.LENGTH_SHORT).show()
//                                holder.addFriendButton.setImageResource(R.drawable.ic_close_black_24dp)
//                            }
//                        }else{
//                            Toast.makeText(this@SuggestedFriendsActivity, "friend request failed to send", Toast.LENGTH_SHORT).show()
//
//                        }
//                    }