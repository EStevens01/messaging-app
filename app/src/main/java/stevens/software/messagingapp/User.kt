package stevens.software.messagingapp

data class User(var id: String? = null, var name: String? = null)
